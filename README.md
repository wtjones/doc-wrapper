# Doc Wrapper

## Overview

An example REST-based gateway for a third-party document portal

## Requirements

* .NET Core 3.1+
* [httpie](https://github.com/jakubroztocil/httpie) (to run example)

## Usage

### Start the App

`dotnet run --project ./src/Api`

or

`dotnet watch --project ./src/Api run`

or

`./run-watch.sh`

### Run a full example

`./run-example.sh`

### Individual Step Examples

#### Request a document task

`http POST localhost:5000/request body="A document body"`

* A reference guid is returned.
* The vendor api is submitted with a callback url of `/callback?requestid={guid}`

#### Mimic the vendor service's inital callback

` echo "STARTED" | http POST localhost:5000/callback Content-Type:text/plain requestid=={guid}`


#### The vendor sends an update

`http PUT localhost:5000/callback status="PROCESSING" detail="a detail" requestid=={guid}`

#### Obtain the status

`http GET http://localhost:5000/status/{guid}`

## Run Tests (incomplete)

`dotnet test test/Api.Tests`

or

`./run-tests.sh`

Tests coverage was not fleshed out due to lack of time. See Testing section below.

## Things to improve in a real application

### Process flow

* If the vendor sends 'STARTED' a 2nd time an error should be returned or maybe raise an alert.
* Use a workflow library to track the process.

### Web Project

* Controllers should be async

### Project structure

* For simplicity, everything is in the _Api_ project, but it could be improved like this:
  * Api
  * Application
    * Features
      * FeatureA
      * FeatureB
  * Domain (depending on need)
  * Gateway
  * Infrastructure

### Infrastructure

* EF Core in-memory is buggy and does not demonstrate relational behavior.
* Use UTC

### Testing

* Tests should use the same IoC container when appropriate.
* I prefer [SliceFixture](https://github.com/jbogard/ContosoUniversityDotNetCore/tree/master/ContosoUniversity.IntegrationTests)-style integration tests for testing application features.
* Tests are lacking in this example, as either slicefixture or a mocking pattern is out of scope time-wise. I prefer to have:
  - A single controller with full test coverage
  - Multiple handlers routed via MediatR

### Patterns

* I prefer not to have persistence concerns in the controller.
  * A MediatR handler in the Application project would be better suited.

## Example Output

### Client

```
======== send inital request
REQUEST_ID is f6fe41ef-8d63-4fe0-b973-02c5655929f6
======== simulate response from vendor api
HTTP/1.1 204 No Content
Content-Length: 0
Date: Fri, 24 Jul 2020 01:36:48 GMT
Server: Kestrel



======== vendor sends an update
HTTP/1.1 204 No Content
Content-Length: 0
Date: Fri, 24 Jul 2020 01:36:50 GMT
Server: Kestrel



======== get status
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Date: Fri, 24 Jul 2020 01:36:51 GMT
Server: Kestrel
Transfer-Encoding: chunked

{
    "body": "a body",
    "createdOn": "2020-07-23T20:36:47.612408-05:00",
    "detail": "a detail",
    "status": "PROCESSED",
    "updatedOn": "2020-07-23T20:36:50.805649-05:00"
}

======== vendor sends another update
HTTP/1.1 204 No Content
Content-Length: 0
Date: Fri, 24 Jul 2020 01:36:53 GMT
Server: Kestrel



======== get status
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Date: Fri, 24 Jul 2020 01:36:54 GMT
Server: Kestrel
Transfer-Encoding: chunked

{
    "body": "a body",
    "createdOn": "2020-07-23T20:36:47.612408-05:00",
    "detail": "a detail",
    "status": "COMPLETED",
    "updatedOn": "2020-07-23T20:36:53.971824-05:00"
}


```

### Server Logs

```
info: DocWrapper.Api.Controllers.RequestController[0]
      requestId: f6fe41ef-8d63-4fe0-b973-02c5655929f6, request.Body: a body
info: DocWrapper.Api.Controllers.RequestController[0]
      Sending callback to vendor api: http://localhost:5000/callback?requestid=f6fe41ef-8d63-4fe0-b973-02c5655929f6
info: DocWrapper.Api.Controllers.CallbackController[0]
      Callback with requestId: f6fe41ef-8d63-4fe0-b973-02c5655929f6, status: STARTED

info: DocWrapper.Api.Controllers.CallbackController[0]
      Found requestId: f6fe41ef-8d63-4fe0-b973-02c5655929f6, status: STARTED

info: DocWrapper.Api.Controllers.CallbackController[0]
      Callback update with requestId: f6fe41ef-8d63-4fe0-b973-02c5655929f6, status: PROCESSED, detail: a detail
info: DocWrapper.Api.Controllers.CallbackController[0]
      Found requestId: f6fe41ef-8d63-4fe0-b973-02c5655929f6
info: DocWrapper.Api.Controllers.StatusController[0]
      Status with requestId: f6fe41ef-8d63-4fe0-b973-02c5655929f6
info: DocWrapper.Api.Controllers.CallbackController[0]
      Callback update with requestId: f6fe41ef-8d63-4fe0-b973-02c5655929f6, status: COMPLETED, detail: a detail
info: DocWrapper.Api.Controllers.CallbackController[0]
      Found requestId: f6fe41ef-8d63-4fe0-b973-02c5655929f6
info: DocWrapper.Api.Controllers.StatusController[0]
      Status with requestId: f6fe41ef-8d63-4fe0-b973-02c5655929f6
```
