#!/bin/sh

echo ======== send inital request
REQUEST_ID=$(http POST localhost:5000/request body="a body")
echo REQUEST_ID is $REQUEST_ID

sleep 1

echo ======== simulate response from vendor api
echo "STARTED" | http POST localhost:5000/callback Content-Type:text/plain requestid==$REQUEST_ID

sleep 1

echo ======== vendor sends an update
http PUT localhost:5000/callback status="PROCESSED" detail="a detail" requestid==$REQUEST_ID

sleep 1

echo ======== get status
http GET http://localhost:5000/status/$REQUEST_ID

sleep 1

echo ======== vendor sends another update
http PUT localhost:5000/callback status="COMPLETED" detail="a detail" requestid==$REQUEST_ID

sleep 1

echo ======== get status
http GET http://localhost:5000/status/$REQUEST_ID
