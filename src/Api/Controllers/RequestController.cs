﻿using System;
using System.ComponentModel.DataAnnotations;
using DocWrapper.Api.Gateway;
using DocWrapper.Api.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DocWrapper.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RequestController : ControllerBase
    {
        private readonly ILogger<RequestController> _logger;
        private readonly IVendorGateway _gateway;
        private readonly DocWrapperContext _context;

        public RequestController(
            ILogger<RequestController> logger,
            IVendorGateway gateway,
            DocWrapperContext context)
        {
            _logger = logger;
            _gateway = gateway;
            _context = context;
        }

        [HttpPost]
        public string SubmitDocument(DocumentRequest request)
        {
            var requestId = Guid.NewGuid();
            _logger.LogInformation(
                $"requestId: {requestId}, request.Body: {request.Body}");

            // TODO: Use a Url builder
            var callbackUrl =
                $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/callback?requestid={requestId.ToString()}";

            _logger.LogInformation($"Sending callback to vendor api: {callbackUrl}");
            _gateway.SubmitDocument(callbackUrl, request.Body);
            _context.DocumentSubmissions.Add(new DocumentSubmission()
            {
                RequestId = requestId,
                Body = request.Body
            });
            _context.SaveChanges();
            return requestId.ToString();
        }
    }

    public class DocumentRequest
    {
        [Required]
        public string Body { get; set; }
    }
}
