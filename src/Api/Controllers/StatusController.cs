﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using DocWrapper.Api.Gateway;
using DocWrapper.Api.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using static DocWrapper.Api.Gateway.Enums;

namespace DocWrapper.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StatusController : ControllerBase
    {
        private readonly ILogger<StatusController> _logger;
        private readonly DocWrapperContext _context;

        public StatusController(
            ILogger<StatusController> logger,
            DocWrapperContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet("{requestId}")]
        public IActionResult Get(string requestId)
        {
            _logger.LogInformation(
                    $"Status with requestId: {requestId}");

            Guid parsedRequestId;
            if (!Guid.TryParse(requestId, out parsedRequestId))
            {
                return BadRequest();
            }
            var submission = _context.DocumentSubmissions.Find(parsedRequestId);
            if (submission == null)
            {
                return NotFound();
            }
            return Ok(new DocumentSubmissionStatus()
            {
                Status = submission.Status,
                Detail = submission.Detail,
                Body = submission.Body,
                CreatedOn = submission.CreatedOn,
                UpdatedOn = submission.UpdatedOn
            });
        }
    }

    public class DocumentSubmissionStatus
    {
        [Required]
        public string Status { get; set; }
        public string Detail { get; set; }
        public string Body { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
