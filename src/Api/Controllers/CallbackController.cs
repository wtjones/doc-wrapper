﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using DocWrapper.Api.Gateway;
using DocWrapper.Api.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DocWrapper.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CallbackController : ControllerBase
    {
        private readonly ILogger<CallbackController> _logger;
        private readonly DocWrapperContext _context;

        public CallbackController(
            ILogger<CallbackController> logger,
            DocWrapperContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpPost]
        public IActionResult InitialCallback(string requestId)
        {
            string status = null;
            using (var reader = new StreamReader(Request.Body))
            {
                status = reader.ReadToEnd();
                _logger.LogInformation(
                    $"Callback with requestId: {requestId}, status: {status}");
            }
            if (status.Trim() != Enums.InitialStatus.ToString())
            {
                return BadRequest();
            }

            Guid parsedRequestId;
            if (!Guid.TryParse(requestId, out parsedRequestId))
            {
                return BadRequest();
            }
            var submission = _context.DocumentSubmissions.Find(parsedRequestId);
            if (submission == null)
            {
                return NotFound();
            }

            _logger.LogInformation(
                $"Found requestId: {requestId}, status: {status}");
            submission.Status = status.Trim();
            submission.UpdatedOn = DateTime.Now;
            _context.Update(submission);
            _context.SaveChanges();

            return Ok(null);
        }

        [HttpPut]
        public IActionResult UpdateCallback(
            [FromQuery(Name = "requestid")] string requestId,
            CallbackUpdate update)
        {
            _logger.LogInformation(
                   $"Callback update with requestId: {requestId}, status: {update.Status}, detail: {update.Detail}");

            Enum.TryParse(update.Status.Trim(), out Enums.DocumentStatus documentStatus);
            if (!Enums.UpdateStatuses.Contains(documentStatus))
            {
                return BadRequest();
            }

            Guid parsedRequestId;
            if (!Guid.TryParse(requestId, out parsedRequestId))
            {
                return BadRequest();
            }
            var submission = _context.DocumentSubmissions.Find(parsedRequestId);
            if (submission == null)
            {
                return NotFound();
            }

            _logger.LogInformation(
                $"Found requestId: {requestId}");
            submission.Status = update.Status;
            submission.Detail = update.Detail;
            submission.UpdatedOn = DateTime.Now;
            _context.Update(submission);
            _context.SaveChanges();

            return Ok(null);
        }
    }

    public class CallbackUpdate
    {
        [Required]
        public string Status { get; set; }
        public string Detail { get; set; }
    }
}
