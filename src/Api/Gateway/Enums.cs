using System.Collections.Generic;

namespace DocWrapper.Api.Gateway
{
    public static class Enums
    {
        public enum DocumentStatus
        {
            STARTED,
            PROCESSED,
            COMPLETED,
            ERROR
        }
        public static DocumentStatus InitialStatus
        {
            get { return DocumentStatus.STARTED; }
        }
        public static List<DocumentStatus> UpdateStatuses
        {
            get
            {
                return new List<DocumentStatus>() {
                    DocumentStatus.PROCESSED,
                    DocumentStatus.COMPLETED,
                    DocumentStatus.ERROR
                };
            }
        }
    }

}
