using System;

namespace DocWrapper.Api.Gateway
{

    /// <summary>
    /// A stubb implementation of a gateway to interact with the vendor's
    /// document service.
    /// </summary>
    public class VendorGatewaySimulator : IVendorGateway
    {
        public void SubmitDocument(string callbackUrl, string body)
        {
            // The actual implementation would post:
            // body: {body}
            // callback: /callback
        }
    }

}
