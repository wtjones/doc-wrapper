using System;

namespace DocWrapper.Api.Gateway
{
    /// <summary>
    /// Represents a client for the vendor's document service.
    /// </summary>
    public interface IVendorGateway
    {
        void SubmitDocument(string callbackUrl, string body);
    }

}
