using Microsoft.EntityFrameworkCore;

namespace DocWrapper.Api.Infrastructure
{
    public class DocWrapperContext : DbContext
    {
        public DocWrapperContext(DbContextOptions<DocWrapperContext> options)
            : base(options)
        { }

        public DbSet<DocumentSubmission> DocumentSubmissions { get; set; }
    }
}
