using System;
using System.ComponentModel.DataAnnotations;

namespace DocWrapper.Api.Infrastructure
{
    public class DocumentSubmission
    {
        [Key]
        public System.Guid RequestId
        { get; set; }
        public string Body { get; set; }
        public string Status { get; set; }
        public string Detail { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }

        public DocumentSubmission()
        {
            CreatedOn = DateTime.Now;
            UpdatedOn = CreatedOn;
        }
    }
}
