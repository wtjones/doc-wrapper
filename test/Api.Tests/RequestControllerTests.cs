using System;
using DocWrapper.Api.Controllers;
using DocWrapper.Api.Gateway;
using DocWrapper.Api.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;
using Xunit;

namespace DocWrapper.Api.Tests
{
    public class RequestControllerTests
    {
        [Fact(Skip = "incomplete")]
        public void SubmitDocument_ShouldReturnReference()
        {
            // Arrange
            var gateway = new VendorGatewaySimulator();
            var optionsBuilder = new DbContextOptionsBuilder<DocWrapperContext>();
            optionsBuilder.UseInMemoryDatabase("Tests");
            var context = new DocWrapperContext(optionsBuilder.Options);

            var sut = new RequestController(
                NullLogger<RequestController>.Instance,
                gateway,
                context);

            // Act
            var response = sut.SubmitDocument(new DocumentRequest() { Body = "Test body" });

            // Assert
            Guid guidResult;
            Assert.True(Guid.TryParse(response, out guidResult));
        }
    }
}
